
public class Affichage {
	private static String language = "en";
	public static String possibiliteValidation[] = {"yes", "y", "Yes", "Y", "o", "oui", "O", "Oui"};
	
	public static void partieGagne(){
		switch(language){
			case "en":
				System.out.println("Did you win?");
				System.out.println("Enter 1 if you won");
				System.out.println("Enter 2 if you lost");
				System.out.println("Enter 3 if you didn't finish");
				break;
			default:
				System.out.println("Quel est votre résultat?");
				System.out.println("Entrez 1 pour gagné");
				System.out.println("Entrez 2 pour perdu");
				System.out.println("Entrez 3 pour non terminé");
		}
	}
	public static void etatGagnant(){
		switch(language){
		case "en":
			System.out.println ("Congratulation! I knew you could do this!");
			break;
		default:
			System.out.println("Félicitations! Je savais que tu pouvais le faire!");
		}
	}
	public static void etatPerdant(){
		switch(language){
		case "en":
			System.out.println ("Don't worry, next time will be better.");
			break;
		default:
			System.out.println("Ne te décourage pas. Tu feras mieux la prochaine fois!");
		}
	}
	public static void etatNonTermine(){
		switch(language){
		case "en":
			System.out.println("You couldn't finish? No worries!");
			System.out.println("Otherwise, try to complete every Sudoku before to leave."); 
			System.out.println("Then you could compare your classmates' results to yours!");
			break;
		default:
			System.out.println("Tu n'avais plus le temps de compléter le Sudoku? Pas de soucis!");
			System.out.println("Toutefois essaie de compléter tes parties pour comparer tes statistiques avec tes camarades.");
		}
	}
	
	public static void combienDeTemps(){
		switch(language){
			case "en":
				System.out.println("How many minutes did you need to finish?");
				break;
			default:
				System.out.println("En combien de minutes as tu fini?");
		}
	}
	
	public static void niveauDifficulte(){
		switch(language){
			case "en":
				System.out.println("Select a level");
				System.out.println("Enter 1 for beginner");
				System.out.println("Enter 2 for intermediate");
				System.out.println("Enter 3 for advanced");
				break;
			default:
				System.out.println("Choisissez un niveau:");
				System.out.println("Entrez 1 pour débutant");
				System.out.println("Entrez 2 pour intermédiaire");
				System.out.println("Entrez 3 pour avancé");	
		}
	}
	
	public static void tuContinues(){
		switch(language){
			case "en":
				System.out.println("Do you wish to exit the game? Y/N");
				break;
			default:
				System.out.println("Tu veux quitter? O/N");
			}
	}
	
	public static void entrePrenom(){
		switch(language){
			case "en":
				System.out.println("your first name:");
				break;
			default:
				System.out.println("Ton prenom:");
		}
	}
	public static void entreNom(){
		switch(language){
			case "en":
				System.out.println("Your last name:");
				break;
			default:
				System.out.println("Ton nom:");
		}
	}
	public static void entreAge(){
		switch(language){
			case "en":
				System.out.println("Your age:");
				break;
			default:
				System.out.println("Ton age:");
		}
	}
	
	
	public static void nonValide(){
		switch(language){
			case "en":
				System.out.println ("Error. Please enter your information again.");
				break;
			default:
				System.out.println("Votre choix est invalide. Veuillez saisir à nouveau vos informations.");
		}
	}
	public static void tropVieux(){
		switch(language){
		case "en":
			System.out.println ("You are too old to play this game. You must be under 90 years old.");
			break;
		default:
			System.out.println("Votre âge est au-dessus de la limite permise. Veuillez entrer un nombre en dessous de 90.");
		}
	}
	
	public static void tropJeune(){
		switch(language){
		case "en":
			System.out.println ("You are too young to play this game. You must be over 10 years old.");
			break;
		default:
			System.out.println("Vous êtes trop jeune pour jouer. Vous devez être âgé de plus de 10 ans.");
		}
	}
	
	public static void validPrenom(){
		switch(language){
		case "en":
			System.out.println ("Error. Please enter your real first name");
			break;
		default:
			System.out.println("Erreur lors de la saisie. Veuillez entrer votre vrai prénom");
		}
	}
	
	public static void validNom(){
		switch(language){
		case "en":
			System.out.println ("Error. Please enter your real last name");
			break;
		default:
			System.out.println("Erreur lors de la saisie. Veuillez entrer votre vrai nom");
		}
	}
	
	public static void validNiveau(){
		switch(language){
		case "en":
			System.out.println ("Error. Please enter a new choice: 1, 2 or 3.");
			break;
		default:
			System.out.println("Erreur lors de la saisie. Veuillez choisir le niveau 1, 2 ou 3");
		}
	}
	
	public static void niveauDebutant(){
		switch(language){
		case "en":
			System.out.println ("Beginner Level");
			break;
		default:
			System.out.println("Niveau débutant");
		}
	}
	
	public static void niveauIntermediaire(){
		switch(language){
		case "en":
			System.out.println ("Intermediate Level");
			break;
		default:
			System.out.println("Niveau intermédiaire");
		}
	}
	
	public static void niveauAvance(){
		switch(language){
		case "en":
			System.out.println ("Advanced Level");
			break;
		default:
			System.out.println("Niveau avancé");
		}
	}
	
	public static void validTempsTooShort(){
		switch(language){
		case "en":
			System.out.println ("Impossible. It takes at least 1 minute to complete a Sudoku.");
			break;
		default:
			System.out.println("Impossible. Il t'a fallu plus d'une minute pour compléter ton Sudoku.");
		}
	}
	public static void validTempsTooLong(){
		switch(language){
		case "en":
			System.out.println ("Please enter 500 even if you took more than 500 minutes to complete the Sudoku.");
			System.out.println("If you entered the wrong number, please enter the right one.");
			break;
		default:
			System.out.println("Si tu as pris plus de 500 minutes pour compléter le Sudoku, entre tout de même 500.");
			System.out.println("Si tu t'es trompé lors de la saisie, entre à nouveau le nombre de minute qu'il t'a fallu pour compléter la partie.");
		}
	}
	
	public static void aurevoir(){
		switch(language){
		case "en":
			System.out.println ("Good bye!");
			System.out.println("Come back soon!");
			break;
		default:
			System.out.println("Aurevoir!");
			System.out.println("Reviens jouer bientôt");
		}
	}
	
	public static void displayStatistics(){
		switch(language){
		case "en":
			System.out.println("****************************Sudoku Stats*****************************");
		    System.out.println();
		    System.out.println("-------Personal informations");
		    System.out.println("Name: " + Joueur.this.prenomJoueur);
		    System.out.println("First Name: " + Joueur.this.nomJoueur);
		    System.out.println("Age: " + Joueur.this.ageJoueur "years old");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println();
		    System.out.println();
		    System.out.println();
		    System.out.println("---------------------------Detailed Stats--------------------------");
		    System.out.println();
		    System.out.println("------Beginner Level Stats");
		    System.out.println("Amount of unfinished games: " + partieNonTermineFacile());
		    System.out.println("Amount of won games: "  + nbrePartieGagneFacile + ". Average time: " + tempsResolutionFacile() " minutes" );
		    System.out.println("Total amount of games: " + partieJoueeFacile();
		    System.out.println("Percentage of successful games: " + tauxDeReussiteFacile() + "%");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println("------Intermediate Level Stats");
		    System.out.println("Amount of unfinished games: " + partieNonTermineFacile());
		    System.out.println("Amount of won games: "  + nbrePartieGagneFacile + ". Average time: " + tempsResolutionFacile() " minutes" );
		    System.out.println("Total amount of games: " + partieJoueeFacile() +);
		    System.out.println("Percentage of successful games: " + tauxDeReussiteFacile() + "%");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println("------Advanced Level Stats");
		    System.out.println("Amount of unfinished games: " + partieNonTermineFacile());
		    System.out.println("Amount of won games: "  + nbrePartieGagneFacile + ". Average time: " + tempsResolutionFacile() " minutes" );
		    System.out.println("Total amount of games: " + partieJoueeFacile() +);
		    System.out.println("Percentage of successful games: " + tauxDeReussiteFacile() + "%");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println("---------------------------General Stats---------------------------- ");
		    System.out.println("Amount of lost games: " + nbrePartiePerdue());
		    System.out.println("Amount of won games: " + nbrePartieGagne() + ". Average time: " + tempsResolution() + "minutes");
		    System.out.println("Total amount of games: " + partieJouee());
		    System.out.println("Percentage of successful games: " + tauxDeReussite() + "%");
			
				break;
			default:	
			System.out.println("****************************Statistiques Sudoku*****************************");
		    System.out.println();
		    System.out.println("-------Informations personnelles");
		    System.out.println("Nom: " + Joueur.this.nomJoueur);
		    System.out.println("Prénom: " + Joueur.this.prenomJoueur);
		    System.out.println("Âge: " + Joueur.this.ageJoueur + "ans");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println();
		    System.out.println();
		    System.out.println();
		    System.out.println("---------------------------Statistiques détaillées--------------------------");
		    System.out.println();
		    System.out.println("------Statistiques niveau facile");
		    System.out.println("Nombre de parties non terminées: " + partieNonTermineFacile())
		    System.out.println("Nombre de parties gagnées: "  + nbrePartieGagneFacile + ". Temps de résolution moyen: " + tempsResolutionFacile() " minutes" );
		    System.out.println("Nombre total de parties jouées: " + partieJoueeFacile() +);
		    System.out.println("Taux de réussite du joueur: " + tauxDeReussiteFacile() + "%");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println("------Statistiques niveau moyen");
		    System.out.println("Nombre de parties non terminées: " + partieNonTermineMoyen())
		    System.out.println("Nombre de parties gagnées: " + nbrePartieGagneMoyen() + ". Temps de résolution moyen: " + tempsResolutionFacile() " minutes" );
		    System.out.println("Nombre total de parties jouées: " + partieJoueeMoyen() +);
		    System.out.println("Taux de réussite du joueur: " + tauxDeReussiteMoyen() + "%");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println("------Statistiques niveau difficile");
		    System.out.println("Nombre de parties non terminées: " + partieNonTermineDifficile())
		    System.out.println("Nombre de parties gagnées: " + nbrePartieGagneDifficile() + ". Temps de résolution moyen: " + tempsResolutionFacile() " minutes" );
		    System.out.println("Nombre total de parties jouées: " + partieJoueeDifficile() +);
		    System.out.println("Taux de réussite du joueur: " + tauxDeReussiteDifficile() + "%");
		    System.out.println("-------");
		    System.out.println();
		    System.out.println("---------------------------Statistiques globales---------------------------- ");
		    System.out.println("Nombre de parties perdues: " + nbrePartiePerdue());
		    System.out.println("Nombre de parties gagnées: " + nbrePartieGagne() + ". Temps de résolution moyen: " + tempsResolution() + "minutes");
		    System.out.println("Nombre total de parties jouées: " + partieJouee());
		    System.out.println("Taux de réussite du joueur: " + tauxDeReussite() + "%");
		  }

	}

	
}
	