public class Validation {
	private static int minCaractPrenom = 3;
	private static int maxCaractPrenom = 256;
	
	private static int minCaractNom = 3;
	private static int maxCaractNom = 256;
	
	private static int ageMin = 10;
	private static int ageMax = 90;

	private static int[] niveauxPossible = {1,2,3};
	
	private static float tempsMin = 1;
	private static float tempsMax = 500;
	
	private static int[] etatPossible = {1,2,3};
	
	public static Boolean prenom(String prenomJoueur){
		Boolean isOk = new Boolean(true);
		if(prenomJoueur.length() < minCaractPrenom || prenomJoueur.length() > maxCaractPrenom){
			Affichage.validPrenom();
			isOk = false;
		}
		return isOk;
	}
	
	public static Boolean nom(String nomJoueur){
		Boolean isOk = new Boolean(true);
		if(nomJoueur.length() < minCaractNom || nomJoueur.length() > maxCaractNom){
			Affichage.validNom();
			isOk = false;
		}
		return isOk;
	}
	
	public static Boolean age(int monAge){
		Boolean isOk = new Boolean(true);
		if(monAge == -1){
			Affichage.nonValide();
			isOk = false;
		}else if(monAge < ageMin){
			Affichage.tropJeune();
			isOk = false;
		}else if(monAge > ageMax){
			Affichage.tropVieux();
			isOk = false;
		}
		return isOk;
	}
	
	public static Boolean niveau(int monNiveau){
		Boolean isOk = new Boolean(true);
		if (monNiveau == niveauxPossible [0]){
			Affichage.niveauDebutant();
			
		}else if (monNiveau == niveauxPossible [1]){
			Affichage.niveauIntermediaire();
			
		}else if (monNiveau == niveauxPossible [2]){ 
			Affichage.niveauAvance();
		}else {
			Affichage.validNiveau();
			isOk = false;
		}
		return isOk;
	}

	
	public static Boolean temps(float leTemps){
		Boolean isOk = new Boolean(true);
		if (leTemps < 0){
			Affichage.nonValide();
			isOk = false;
		}else if (leTemps < tempsMin){
			Affichage.validTempsTooShort();
			isOk = false;
		}
		else if(leTemps > tempsMax){
			Affichage.validTempsTooLong();
			isOk = false;
		}
		return isOk;

	}
	
	public static Boolean partie(int unEtat){
		Boolean isOk = new Boolean(true);
		if (unEtat == -1){
			Affichage.nonValide();
		}else if (unEtat == etatPossible [0]){
			Affichage.etatGagnant();
		}else if (unEtat == etatPossible [1]){
			Affichage.etatPerdant();
		}else if (unEtat == etatPossible [2]){ 
			Affichage.etatNonTermine();
		}else {
			Affichage.nonValide();
			isOk = false;
		}

		return isOk;
	}
	public static Boolean continuing(String continu){
		Boolean isOk = new Boolean(false);
		for(int i = 0; i < Affichage.possibiliteValidation.length; i++){
			if (continu == Affichage.possibiliteValidation[i]) {
					isOk = true;
					break;
			}
		}
		return isOk;
	}

}
