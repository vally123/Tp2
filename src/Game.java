public class Game{
	  private int niveau;
	  private float temps;
	  private int etat;
	  
	  Game(){
		  this.niveau = -1;
		  this.temps = -1;
		  this.etat = -1;
	  }
	  
	  public int getNiveau(){
	    return this.niveau;
	  }
	  public void setNiveau(int monNiveau){
		  this.niveau = monNiveau;
	  }
	  
	  public float getTemps(){
	    return this.temps;
	  }
	  public void setTemps(float monTemps){
		  this.temps = monTemps;
	  }
		  
		  
	  public int getEtat(){
		    return this.etat;
	  }
	  public void setEtat(int monEtat){
		  this.etat = monEtat;
	  }
}