import java.util.Scanner;
import java.util.ArrayList;
import java.util.InputMismatchException;

public class Joueur{
	// Attributs
	public String prenomJoueur;
	public String nomJoueur;
	public int ageJoueur;
	public ArrayList<Game> mesParties;

	// Constructeur
	Joueur(){
		this.prenomJoueur = "";
		this.nomJoueur = "";
		this.ageJoueur = 0;
		this.mesParties = new ArrayList<Game>();
	}
	// Méthodes
	
	//Le joueur saisi ses informations personnelles
	public void enregistrerLeJoueur(){
		// saisie du prenom
		do{
			Scanner choix  = new Scanner (System.in);
			Affichage.entrePrenom();
			this.prenomJoueur = choix.nextLine();
		}while(Validation.prenom(this.prenomJoueur) == false);
		
	
		// saisie du nom
		do{
			Scanner choix  = new Scanner (System.in);
			Affichage.entreNom();
			this.nomJoueur = choix.nextLine();
		}while(Validation.nom(this.nomJoueur) == false);

		// saisie de l'age
		do{
			Scanner choix  = new Scanner (System.in);
			Affichage.entreAge();
			try{
				this.ageJoueur = choix.nextInt();
			}catch(InputMismatchException err){
				this.ageJoueur = -1;
			}
		}while(Validation.age(this.ageJoueur) == false);
	}
	
	
	// Le joueur enregistre les informations d'une partie
	public void enregistrerUnePartie(){
		Game monGame = new Game();
		
		// saisie du niveau de difficulté
		int choixNiveau;
		do{
			Scanner choix  = new Scanner (System.in);
			Affichage.niveauDifficulte();
			
			try{
				choixNiveau = choix.nextInt();
			}catch(InputMismatchException err){
				choixNiveau = -1;
			}
			monGame.setNiveau(choixNiveau);
		}while(Validation.niveau(monGame.getNiveau()) == false);
		
		// état de la partie
		int etat;
		do{
			Scanner choix  = new Scanner (System.in);
			Affichage.partieGagne();
			try{
				etat = choix.nextInt();
			}catch(InputMismatchException err){
				etat = -1;
			}
			monGame.setEtat(etat);
		}while(Validation.partie(monGame.getEtat()) == false );
		
		// si la partie est gagné, en combien de temps?
		float temps;
		if(etat == 1){
			do{
				Scanner choix  = new Scanner (System.in);
				Affichage.combienDeTemps();
				try{
					temps = choix.nextFloat();
				}catch(InputMismatchException err){
					temps = -1;
				}
				monGame.setTemps(temps);
			}while(Validation.temps(monGame.getTemps()) == false);		
		}
		
		//System.out.println(monGame());
		this.mesParties.add(monGame);
	}
	
	// Quitte le jeu
	public Boolean quitterLeJeu(){
			Boolean arreterJeu = new Boolean(false);
			Affichage.tuContinues();
			Scanner choix = new Scanner (System.in);
				// TODO a tester !!!!
				arreterJeu = choix.nextBoolean();
			return arreterJeu;
	}
	
	
	// Affiche les statistiques du la partie courant
	public void afficherStatistiquesSudoku(){
		//Statistics.computeStats(this.mesParties);
	}
}

